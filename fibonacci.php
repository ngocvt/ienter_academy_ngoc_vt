<?php

function fibonacci($n) {
    if ($n < 2) {
        return $n;
    } else {
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}

var_dump(fibonacci(3));
var_dump(fibonacci(4));
var_dump(fibonacci(5));
var_dump(fibonacci(6));
var_dump(fibonacci(7));
