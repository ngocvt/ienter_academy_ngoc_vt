<?php
//Bubble Sort
$mang_1 = [1, 5, 9, 2, 4, 0];
function bubble_sort($mang){
    for ($i = 0; $i < (count($mang)- 1); $i++)
    {
        for ($j = $i + 1; $j < count($mang); $j++)
        {
            if ($mang[$i] > $mang[$j])
                {
                    $tmp = $mang[$j];
                    $mang[$j] = $mang[$i];
                    $mang[$i] = $tmp;
                }
        }
    }
    for ($i=0; $i < count($mang); $i++) { 
        echo $mang[$i];
    }
}
bubble_sort($mang_1);

echo "<br>";

//Selection sort
$mang_2 = [1, 5, 9, 2, 4, 0, 8, 3, 7];

function selection_sort($mang)
{   
    $min = 0;
    for($i=0; $i<count($mang); $i++){
        $min = $i;
        for($j = $i+1; $j < count($mang); $j++   ){
            if($mang[$j] < $mang[$min]){
                $min = $j;
            }
        }

        $tmp = $mang[$min];
        $mang[$min] = $mang[$i];
        $mang[$i] = $tmp;
    }

    for ($i=0; $i < count($mang); $i++) { 
        echo $mang[$i];
    }
}

selection_sort($mang_2);

//QuickSort

echo "<br>";
function quick_sort($mang)
{
    if(count($mang) <= 1){
        return $mang;
    }
    else{
        $pivot = $mang[0];
        $left = array();
        $right = array();
        for($i = 1; $i < count($mang); $i++)
        {
            if($mang[$i] < $pivot){
                $left[] = $mang[$i];
            }
            else{
                $right[] = $mang[$i];
            }
        }
        return array_merge(quick_sort($left), array($pivot), quick_sort($right));
    }
}

var_dump(quick_sort($mang_2));