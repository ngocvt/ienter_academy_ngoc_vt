<?php

function linearSearch($list, $n){
    $count = count($list);

    for($i = 0; $i < $count; $i++){
        if($n == $list[$i]){
            echo $n." có xuất hiện, tại vị trí ".$i;
        }
    }
}
$list_1 = [1,3,2,4,6,7,5,8];

linearSearch($list_1, 3);
echo '<br>';

$list_2 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];

function binarySearch($n,$list){
    $start = 0;
    $end = count($list) - 1;
    while($start <= $end ){
        $mid = floor(($start + $end) / 2);
        if($n < $list[$mid]){
            $end = $mid - 1;
        }elseif($n > $list[$mid]){
            $start = $mid + 1;
        }elseif($n == $list[$mid]){
            return 'index=='.$mid.' and value=='.$n;
        }
    }
    return "not found";
}

var_dump(binarySearch( 28 ,$list_2));

